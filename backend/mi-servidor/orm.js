const { Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');
const initModels = require("./models/init-models");
const sequelize = new Sequelize('mysql://root:root@localhost:3306/Autenticación')
const dbContext = initModels(sequelize);

//console.log(`Inicio ${new Date().toLocaleTimeString('es')}`)
//dbContext.language.findOne({where: { language_id: {[Op.lt]:10} }}).then(row => console.log(row.toJSON()))

async function conUno(id) {
    let row = await dbContext.Usuarios.findOne({ where: { idUsuarios: { [Op.eq]: id } } })
    // let row = await dbContext.actor.findByPk(1)
    return row.toJSON()
    //console.log(row.toJSON())
}
// async function conMuchos() {
//     let rows = await dbContext.Usuarios.findAll({ where: { idUsuarios: { [Op.gt]: 1} } })
//     rows.forEach(row => {
//         console.log(row.toJSON())
//     });
// }
// async function conPaginas(page=0, limit=40) {
//     let rows = await dbContext.Usuarios.findAll({ offset: page*limit, limit , order: ['name'] })
//     console.log(rows.map(row => ({id: row.idUsuarios, name: row.nombre})))
// }
// async function conAsociaciones() {
//     // let row = await dbContext.actor.findByPk(1)
//     // let rows = await row.getPeliculas()
//     let row = await dbContext.language.findByPk(2, { include: 'films' })
//     let rows = row.films 
//     console.log({id: row.idUsuarios, name: row.nombre, Roles: rows.map(item => ({id: item.idRol, name: item.title}))})
// }
// async function insert() {
//     let row = await dbContext.Usuarios.build({name: 'Prueba'})
//     // if(!row.validate()) {
    //     console.log('400 Datos invalidos')
    //     return;
    // }
    // // let pelis = await dbContext.film.findAll({ where: { film_id: { [Op.lt]: 4 } } })
    // // pelis.forEach(item => row.).createPelicula({actor_id})
    // // row.createPelicula(await dbContext.film.findAll({ where: { film_id: { [Op.lt]: 4 } } }))
    // console.log(row)
    // console.log('Guardo ...')
    // await row.save()
//     // let row = await dbContext.actor.create({first_name: 'Pepito', last_name: 'Grillo'})
//     console.log(row)
//     await conMuchos()
// }
// async function update() {
//     let row = await dbContext.Usuarios.findOne({ order: [['idUsuarios','DESC']] })
//     console.log(row)
//     row.name = row.name.toUpperCase()
//     console.log('Guardo ...')
//     await row.save()
//     console.log(row)
//     // await conMuchos()
// }
// async function remove() {
//     let row = await dbContext.Usuarios.findOne({ order: [['idUsuarios','DESC']] })
//     console.log(row)
//     await row.destroy()
//     console.log(row)
//     await conMuchos()
// }
module.exports.conUno = conUno

//conUno().then(()=> { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0)}, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

// conMuchos().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
// conPaginas(1, 20).then(()=> { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0)}, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
// conAsociaciones().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
// insert().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
// update().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
// remove().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

// '/actores/:id'
// await sequelize.query('DELETE FROM `actor` WHERE `actor_id` = ' + req.params.id)
// DELETE http://localhost:4200/actores/1%20or%201=1
// [
//     '208',
//     '1 or 1=1',
//     '1; DROP TABLE ...',
//     '1; exec xp_cmdShell("FDISK ...")'
// ].forEach(id => console.log('delete from contactos where idContacto=' + +id))

// await sequelize.query('DELETE FROM `actor` WHERE `actor_id` = $id', { bind: {id:1}, type: QueryTypes.SELECT });

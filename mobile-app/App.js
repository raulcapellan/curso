import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
import Demos from './components/demos'

export default function App() {
  return (
    <Demos /> 
  );
}

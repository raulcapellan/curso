
import React from 'react';
 
import OutputScreenRow from './outputRow.js';
 
const OutputScreen = (props) => {
  return (
    <div className="screen">
      <OutputScreenRow value = {props.question}/>
      <OutputScreenRow value = {props.answer}/>
    </div>
  )
}
 
// Export Output Screen.
export default OutputScreen;
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import ButtonPanel from './buttonpanel2.js';
import Display from './display2.js';
import calculate from './calcula2';
import {faCalculator} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import '../calculadoras.css'
class Calculador extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      next: null,
      operation: null,
      total: null,
    };
  }

  handleClick(buttonName) {
    const { next, operation, total } = calculate(this.state, buttonName);
    this.setState({
      next,
      operation,
      total,
    });
  }

  render() {
  
    let value = null;
    if (this.state.operation === null) {
      value = this.state.total ? this.state.total : '0';
    } else {
      value = this.state.next ? this.state.next : this.state.total;
    }

    return (
      <div>
        <div className="calculad" style={{fontSize: '5em', width: '100%', textAlign: 'center'}}>
        <FontAwesomeIcon icon={faCalculator}/>
        </div>
        <Display value={value} />
        <ButtonPanel onClick={buttonName => this.handleClick(buttonName)} />
      </div>
    );
  }
}

export default Calculador;
import React, {Component} from 'react';
import './galeria.css'
import PropTypes from 'prop-types';
import { useState } from 'react';

export class Contador extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contador: +props.init,
            delta: +props.delta
        };
        this.baja = this.baja.bind(this)
        this.init = this.init.bind(this)
        this.sube = (e) => {
            e.preventDefault();
            this.setState((prev, props) => {
                const cont = prev.contador + prev.delta
                this.notifyChange(cont)
                return { contador: cont }
            })
            };

    }
    baja(e) {
        e.preventDefault();
        this.setState((prev, props) => {
            const cont = prev.contador - prev.delta
            this.notifyChange(cont)
            return { contador: cont }
        })
    }

    init(value, e) {
        e.preventDefault();
        this.setState({ contador: value })
        this.notifyChange(value)
        // this.state.contador = value
    }
    notifyChange(value) {
        if(this.props.onChange) {
            this.props.onChange(value)
        }
    }
    render() {
        return (
            <div>
                <h1>{this.state.contador}</h1>
                <p>
                    <button onClick={this.sube}>Sube</button>&nbsp;
                    <button onClick={this.baja}>Baja</button>
                </p>
            </div>
        );
    }
}
Contador.propTypes = {
    init: PropTypes.number.isRequired,
    delta: PropTypes.number,
    onChange: PropTypes.func
};
Contador.defaultProps = {
    delta: 1
};

import React, { Component } from 'react';
import GalleryImage  from '../Componentes/Imagen.js';
import GalleryModal from '../Componentes/Modal.js';
import    '../galeria.css';

let imgUrls = ['https://placeimg.com/640/480/any',]

export default class Gallery extends Component{
    constructor(props) {
      super(props);
      
      this.state = {
        isOpen: false,
        url: ''
      }
      
  }
    
    render() {
      let elements = [...Array(100)].map((elements, index) => (
        <GalleryImage key={index} className='gallery-thumbnail' src={'https://picsum.photos/id/'+index+'/200/200'} alt={'Imagen número ' + (index + 1)} size={this.props.size} />
      ));

      return(
        <div  className='container-fluid gallery-container'>
          {elements}
        </div>
      )
    }

        
    // Function for opening modal dialog
    // I change the function to a arrow functions to avoid to declare the binding in the constructor
    openModal = (url) => {
     
       this.setState({
         isOpen: true,
         url: url
       });
     }
  
    // Function for closing modal dialog
    // I change the function to a arrow functions to avoid to declare the binding in the constructor
    closeModal = () => { 
      
       this.setState({
        isOpen: false,
        url: ''
      });
    }
  }
 
import React, { Component } from 'react';

export default class GalleryImage extends Component {
    constructor(props) {
        super(props)
        this.state = {boton: true}
        this.cambia=this.cambia.bind(this)
    }

    cambia () {this.setState((state, props)=>{return {boton:false};});}
    
    render() 
    
    { 
        let element;
        if(this.state.boton){
                element=<button onClick={this.cambia}>boton</button>
        } else{
            element=<img style={{width:this.props.size+'px'}} className={this.props.className} src={this.props.src} alt={this.props.alt} />
        }

        return ( 
           {element}     
        )
    }
    }

import React, { Component } from 'react';
import Gallery from './Generadores/muro.js';
import { Contador } from './tamanoimagenes.js';

class Imag extends Component {
    constructor(props){
        super(props)
        this.state={tamaño: 64}
    }
  render() {
    return (
      <div>
          <div className='ctr' style={{textAlign: 'center', width: '100%', color: 'green'}}>
          <Contador init={this.state.tamaño} delta={32} tamaño={this.state.tamaño} onChange={tamaño=>this.setState({tamaño})}/>
          </div>
          <Gallery size={this.state.tamaño}/> 
       </div>
    );
  }
}

export default Imag;
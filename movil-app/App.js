import { StatusBar } from 'expo-status-bar';
import { SafeAreaView, StyleSheet, Text, View, Button } from 'react-native';
import CalculadoraScreen from './components/Calculadora2/Pantalla';
import Calculadora from './components/calculator'
import {styles} from './components/Calculadora2/Estilo'

export default function App() {
  return (
    <SafeAreaView style={styles.background}>
      <StatusBar 
        backgroundColor='black'
        barStyle="light-content"
      />
      <CalculadoraScreen />
    </SafeAreaView>
  );
}
